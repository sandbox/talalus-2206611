<?php

/**
 * @file
 * Provides the hsoub_captcha administration settings.
 */

/**
 * Form callback; administrative settings for hsoub_captcha.
 */
function hsoub_captcha_settings_form() {
  $form = array();
  $form['hsoub_captcha_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#default_value' => variable_get('hsoub_captcha_api_key', ''),
    '#maxlength' => 40,
    '#description' => t('The API key given to you when you <a href="http://captcha.hsoub.com/signup" target="_blank">registered at hsoub.com</a>.'),
    '#required' => TRUE,
  );
  $form['hsoub_captcha_theme_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Theme Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['hsoub_captcha_theme_settings']['hsoub_captcha_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Background color'),
    '#default_value' => variable_get('hsoub_captcha_background_color', 'ffffff'),
    '#maxlength' => 6,
    '#size' => 6,
    '#description' => t('HEX (without #)'),
  );  
  $form['hsoub_captcha_theme_settings']['hsoub_captcha_border_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Border color'),
    '#default_value' => variable_get('hsoub_captcha_border_color', 'c5dbec'),
    '#maxlength' => 6,
    '#size' => 6,
    '#description' => t('HEX (without #)'),
  ); 

  return system_settings_form($form);
}